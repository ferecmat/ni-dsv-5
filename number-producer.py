#!/usr/bin/env python3
import pika
import sys

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()
channel.queue_declare(queue='number_queue', durable=True)

channel.basic_publish(
    exchange='',
    routing_key='number_queue',
    body=sys.argv[1],
    properties=pika.BasicProperties(
        delivery_mode=2,
    ),
)
connection.close()
