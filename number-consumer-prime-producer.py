#!/usr/bin/env python3
import pika
import time

credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.queue_declare(queue='number_queue', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')


def is_prime(n):
    try:
        x = int(n)
        if x < 2:
            return False

        for i in range(2, int(x**0.5) + 1):
            if x % i == 0:
                return False
        return True
    except ValueError:
        return False


def send():
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='primes')

    def callback(ch, method, properties, body):
        print(' [x] Received %r' % body)
        if is_prime(body):
            print(f' [x] {body} - is prime')
            channel.basic_publish(exchange='', routing_key='primes', body=body)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    return callback


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='number_queue', on_message_callback=send())
channel.start_consuming()
